import React from 'react';
import { ScrollView, Image, StyleSheet, Text, View } from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";

import HomeScreen from './screen/home'
import MainMenuScreen from './screen/mainmenu';
import LonginScreen from './screen/login';
import TimingScreen from './screen/timing';
import SucceedScreen from './screen/succeed';

const AppNavigator = createStackNavigator(
  {
    Login: LonginScreen,
    Timing: TimingScreen,
    Succeed: SucceedScreen,
    Home: HomeScreen,
    Main: MainMenuScreen,
  }, 
  {
    initialRouteName: "Login",   
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: 'rgba(51, 153, 255,0.5)',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
        fontSize:18,
      },
    }
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return (
      <AppContainer />
    )

  }
}



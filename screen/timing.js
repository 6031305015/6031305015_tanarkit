import React from 'react';
import { FlatList, ScrollView, Image, StyleSheet, Text, View } from 'react-native';
// import HeaderBar from '../components/header';
import Card from '../components/card'
import ListView from '../components/listview'

import { TouchableOpacity } from 'react-native-gesture-handler';
import mydata from '../data/timedoc1';


export default class TimingScreen extends React.Component {
    static navigationOptions = {
        title: 'Time'
    };

    constructor(props) {
        super(props)
        this.state = { data: mydata, loading: false }
    }


    loadData = async () => {
        const res =
            await fetch('timedoc1.json')

        const netdata = await res.json()
        console.log(netdata)
        this.setState({ data: netdata })

    }

    async componentDidMount() {
        await this.loadData()
    }render() {


        return (
            <ScrollView>
                <FlatList
                    data={this.state.data}
                    renderItem={({ item }) =>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Home')}>

                            <ListView title={item.title}
                                desc={item.desc}
                                img={{ uri: item.picture }} />

                        </TouchableOpacity>
                    }
                />


            </ScrollView>
        );
    }
}




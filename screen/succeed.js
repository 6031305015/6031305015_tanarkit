import React from 'react';
import { ScrollView, Image, StyleSheet, Text, View } from 'react-native';
import HeaderBar from '../components/header';
import Card from '../components/card'
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Succeed extends React.Component {
  render() {
    let pic = { uri: 'https://mbanogmat.com/wp-content/uploads/2017/01/succeed.jpg' }

    return (
      <ScrollView>
         <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Login')}>
          <Card img={pic} title='Finish' />
        </TouchableOpacity>
      </ScrollView>
    );
  }
}
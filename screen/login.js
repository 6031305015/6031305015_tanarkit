import React from 'react';
import { ScrollView, Image, StyleSheet, Text, View, TextInput, Button } from 'react-native';
import HeaderBar from '../components/header';
import Cardlo from '../components/cardlo'
import { TouchableOpacity } from 'react-native-gesture-handler';


export default class login extends React.Component {
    render() {
        let pic1 = { uri: 'https://www.bbcgoodfood.com/sites/default/files/guide/guide-image/2018/07/hydration-guide-main-image-700-350.jpg' }
        return (
            <ScrollView>
                <HeaderBar headtitle='Drink my Water' />
                <Text style={{ fontSize: 18, fontWeight: 'bold', padding: 10 }} >Name</Text>
                <TextInput style={{ fontSize: 18, padding: 10 }}
                    autoCorrect={false}
                    placeholder='Name/Nickname'></TextInput>

                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Main')}>
                    <Cardlo img={pic1} title='Login' />
                </TouchableOpacity>

            </ScrollView>
        );
    }
}
